package de.laura.tasks.blocks;

import org.json.JSONException;
import org.json.JSONObject;

public class JsBlock extends Block {
    String code;

    @Override
    public JSONObject serialize() throws JSONException {
        JSONObject object = new JSONObject();
        object.put("code", code);
        return object;
    }

    @Override
    public void deserialize(JSONObject object) throws JSONException {
        code = object.getString("code");
    }

    @Override
    public String emit() {
        return code;
    }
}
