package de.laura.tasks.blocks;

import org.json.JSONException;
import org.json.JSONObject;

public class NotificationBlock extends Block {
    String content;
    boolean makeLong;

    @Override
    public JSONObject serialize() throws JSONException {
        JSONObject object = new JSONObject();
        object.put("content", content);
        object.put("long", makeLong);
        return object;
    }

    @Override
    public void deserialize(JSONObject object) throws JSONException {
        content = object.getString("content");
        makeLong = object.getBoolean("long");
    }

    @Override
    public String emit() {
        return "notifications.toast" + (makeLong ? "Long" : "") + "(" + content + ");";
    }
}
