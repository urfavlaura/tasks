package de.laura.tasks.blocks;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class Block {
    public abstract JSONObject serialize() throws JSONException;
    public abstract void deserialize(JSONObject object) throws JSONException;
    public abstract String emit();
}
