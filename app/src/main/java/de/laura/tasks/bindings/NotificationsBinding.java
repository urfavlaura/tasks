package de.laura.tasks.bindings;

import android.widget.Toast;

import com.caoccao.javet.annotations.V8Function;
import com.caoccao.javet.exceptions.JavetException;
import com.caoccao.javet.interop.V8Runtime;

import de.laura.tasks.service.TasksService;

public class NotificationsBinding extends Binding {
    public NotificationsBinding(V8Runtime runtime, TasksService service) throws JavetException {
        super(runtime, service);
    }

    @V8Function
    public void sendToast(String content) {
        Toast.makeText(service, content, Toast.LENGTH_SHORT).show();
    }

    @V8Function
    public void sendToastLong(String content) {
        Toast.makeText(service, content, Toast.LENGTH_LONG).show();
    }

    @Override
    String getObjectName() {
        return "notifications";
    }
}
